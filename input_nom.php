<!doctype html>
<html>
<head>
	<title>Modification d'un joueur</title>
	<meta charset>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
      <nav>
        <ul>
          <li><a class="active" href="nom.php">Joueurs</a></li>
          <li><a href="mot.php">Mots</a></li>
        </ul>
         <div class="home_b">
            <a href="index.php">
               <img src="home.png">
            </a>
         </div>
      </nav>

      <form action="update_nom.php" method="GET">
         <label for="nom">Modifier nom du joueur par :</label>
         <input name="nom" type="text" placeholder="Votre nouveau nom" tabindex="1" autocomplete="off">
         <input type="hidden" name="id" value="<?php echo $_GET["id"]; ?>" >
         <input type="submit">
      </form>
  
   <br>
<a href="nom.php">Retour</a>
</body>
</html>
