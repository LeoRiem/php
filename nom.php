<!doctype html>
<html>
<head>
	<title>Joueurs</title>
	<meta charset>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
      <nav>
        <ul>
          <li><a class="active" href="nom.php">Joueurs</a></li>
          <li><a href="mot.php">Mots</a></li>
        </ul>
         <div class="home_b">
            <a href="index.php">
               <img src="home.png">
            </a>
         </div>
      </nav>
   <form action="create_nom.php" method="GET">
      <label for="nom">Nouveau joueur</label>
      <input name="nom" type="text" placeholder="Nouveau joueur" tabindex="1" autocomplete="off">
      <input type="submit">
   </form>
	<ul>
<?php
	ini_set("display_errors","true");
	$handle = mysqli_connect("localhost","root","1234","penduApp");
	$query="SELECT * FROM joueurs";
	$result = mysqli_query($handle,$query);

	while($line=mysqli_fetch_array($result)) {
		echo "\t<li>" . $line["id"] . "&nbsp;" . $line["nom"];
		echo "&nbsp;<a href=\"delete_nom.php?id=" . $line["id"] . "\"><span>X</span></a>";
		echo "&nbsp;<a href=\"input_nom.php?id=" . $line["id"] . "\">UPDATE</a>";
		echo "</li>\n";
	}

?>
	</ul>
</body>
</html>
